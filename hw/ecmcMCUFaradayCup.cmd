# Faraday Cup EtherCAT HW configuration

# 0/9 /18/27 + EK1100 EtherCAT Coupler (2A E-Bus)
# 1/10/19/28 + EL1808 8K. Dig. Eingang 24V, 3ms
# 2/11/20/29 + EL2819 16K. Dig. Ausgang 24V, 0, 5A, Diagnose
# 3/12/21/30 + EL3214 4K. Ana. Eingang PT100 (RTD)
# 4/13/22/31 + EL3174-0002 4K. Ana. Eingang +/-10V, +/-20mA, 16 Bit, Galvanis
# 5/14/23/32 + EL4134 4K. Ana. Ausgang -10/+10V. 16bit
# 6/15/24/33 + EL9505 Netzteilklemme 5V
# 7/16/25/34 + EL2124 4K. Dig. Ausgang 5V, 20mA
# 8/17/26/35 + EK1110 EtherCAT-Verl�ngerung

# Configure EK1100 coupler terminal
$(SCRIPTEXEC) $(ecmccfg_DIR)/addSlave.cmd "HW_DESC=EK1100"

# Configure EL1808 digital input terminal
$(SCRIPTEXEC) $(ecmccfg_DIR)/addSlave.cmd "HW_DESC=EL1808"

# Configure EL2819 digital output terminal
$(SCRIPTEXEC) $(ecmccfg_DIR)/addSlave.cmd "HW_DESC=EL2819, SUBST_FILE=$(E3_CMD_TOP)/db/ecmcEL2819_FC.substitutions"

# Configure EL3214 4K. Ana. Eingang PT100 (RTD)
# NOTE: There is nothing connected to this modeule.
$(SCRIPTEXEC) $(ecmccfg_DIR)/addSlave.cmd "HW_DESC=EL3214"

# Configure EL3174 analog input configured to read 0 to 10V in channels 0, 1 and 0 to 20mA in channels 2, 3.
# NOTE: Use cutsom substitution file (ecmcEL7134__FC.substitutions)!
$(SCRIPTEXEC) $(ecmccfg_DIR)/addSlave.cmd "HW_DESC=EL3174, SUBST_FILE=$(E3_CMD_TOP)/db/ecmcEL3174_FC.substitutions"

epicsEnvSet ECMC_EC_HWTYPE "EL3174_0to10V"
epicsEnvSet ECMC_EC_SDO_INDEX "0x800D"
$(SCRIPTEXEC) $(ecmccfg_DIR)/ecmc$(ECMC_EC_HWTYPE)-Sensor-chX.cmd

epicsEnvSet ECMC_EC_HWTYPE "EL3174_0to10V"
epicsEnvSet ECMC_EC_SDO_INDEX "0x801D"
$(SCRIPTEXEC) $(ecmccfg_DIR)/ecmc$(ECMC_EC_HWTYPE)-Sensor-chX.cmd

epicsEnvSet ECMC_EC_HWTYPE "EL3174_4to20mA"
epicsEnvSet ECMC_EC_SDO_INDEX "0x802D"
$(SCRIPTEXEC) $(ecmccfg_DIR)/ecmc$(ECMC_EC_HWTYPE)-Sensor-chX.cmd

epicsEnvSet ECMC_EC_HWTYPE "EL3174_4to20mA"
epicsEnvSet ECMC_EC_SDO_INDEX "0x803D"
$(SCRIPTEXEC) $(ecmccfg_DIR)/ecmc$(ECMC_EC_HWTYPE)-Sensor-chX.cmd

# Configure EL4134 analog output terminal 0-10V.
# NOTE: Use cutsom substitution file (ecmcEL4134_FC.substitutions)!
$(SCRIPTEXEC) $(ecmccfg_DIR)/addSlave.cmd "HW_DESC=EL4134, SUBST_FILE=$(E3_CMD_TOP)/db/ecmcEL4134_FC.substitutions"

# Configure EL9505 power supply 5V
$(SCRIPTEXEC) $(ecmccfg_DIR)/addSlave.cmd "HW_DESC=EL9505"

# Configure EL2124 digital output terminal 5V.
$(SCRIPTEXEC) $(ecmccfg_DIR)/addSlave.cmd "HW_DESC=EL2124, SUBST_FILE=$(E3_CMD_TOP)/db/ecmcEL2124_FC.substitutions"

# Configure EK1110 coupler terminal
$(SCRIPTEXEC) $(ecmccfg_DIR)/addSlave.cmd "HW_DESC=EK1110"
