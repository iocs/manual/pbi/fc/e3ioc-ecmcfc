# EtherCAT Motion Control NCL Faraday Cups (LEBT, MEBT, DTL2 & DTL4) IOC configuration

# Initiation
epicsEnvSet IOC "$(IOC=PBI-FC01:Ctrl-ECAT-100)"
epicsEnvSet SCRIPTEXEC "$(SCRIPTEXEC=iocshLoad)"

require essioc
require ecmccfg 8.0.0

epicsEnvSet LEBTFC LEBT-010:PBI-FC-001
epicsEnvSet MEBTFC MEBT-010:PBI-FC-001
epicsEnvSet DTL2FC DTL-020:PBI-FC-001
epicsEnvSet DTL4FC DTL-040:PBI-FC-001

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(SCRIPTEXEC) "$(ecmccfg_DIR)/startup.cmd" "ECMC_VER=8.0.2, NAMING=ESSnaming"

# Configure hardware
$(SCRIPTEXEC) "$(E3_CMD_TOP)/hw/ecmcMCUFaradayCup.cmd"  # Configure LEBT FC HW
$(SCRIPTEXEC) "$(E3_CMD_TOP)/hw/ecmcMCUFaradayCup.cmd"  # Configure MEBT FC HW
$(SCRIPTEXEC) "$(E3_CMD_TOP)/hw/ecmcMCUFaradayCup.cmd"  # Configure DTL2 FC HW
$(SCRIPTEXEC) "$(E3_CMD_TOP)/hw/ecmcMCUFaradayCup.cmd"  # Configure DTL4 FC HW

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

# ADDITIONAL SETUP

# Set 24 volts to Limit Switch common
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(2,binaryOutput01, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(11,binaryOutput01, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(20,binaryOutput01, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(29,binaryOutput01, 1)"

# Vacuum relay common
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(2,binaryOutput05, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(11,binaryOutput05, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(20,binaryOutput05, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(29,binaryOutput05, 1)"

# Temp transmitter common
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(2,binaryOutput04, 1)"  # Body temperature
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(2,binaryOutput08, 1)"  # Repeller temperature (only in LEBT)
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(11,binaryOutput04, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(20,binaryOutput04, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(29,binaryOutput04, 1)"

# Move permit common
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(2,binaryOutput07, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(11,binaryOutput07, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(20,binaryOutput07, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(29,binaryOutput07, 1)"

# Flow sensor common
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(2,binaryOutput06, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(11,binaryOutput06, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(20,binaryOutput06, 1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(29,binaryOutput06, 1)"

# Set 24 volts to Collision Protection feed
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(2,binaryOutput09, 1)"

dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_HV.db" "IOC=$(IOC), HV=$(LEBTFC), SN3174=s004, SN4134=s005, SN2124=s007, VOLT_LOW=-981, VOLT_LOLO=-990, VOLT_DRVL=-999"
dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_MCU.db" "IOC=$(IOC), MCU=$(LEBTFC), SN1808=s001, SN2819=s002"
dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_MPSsignals.db" "IOC=$(IOC), Sys=$(LEBTFC), SN1808=s001, SN3174=s004, REPEL="
dbLoadRecords "$(E3_CMD_TOP)/db/FC_Vac_Bias_ilock.db" "HV=$(LEBTFC), PORT=MC_CPU1, ASYN_NAME=plcs.plc0.static.LEBTreqbias"

$(SCRIPTEXEC) "$(ecmccfg_DIR)/loadPLCFile.cmd" "PLC_ID=0, SAMPLE_RATE_MS=10, FILE=$(E3_CMD_TOP)/plc/FC_LEBT_vacuum-HV_ilock.plc"
dbLoadRecords "$(E3_CMD_TOP)/db/limitswitchalarm.db" "P=$(LEBTFC)"

$(SCRIPTEXEC) "$(ecmccfg_DIR)/loadPLCFile.cmd" "PLC_ID=4, SAMPLE_RATE_MS=1, FILE=$(E3_CMD_TOP)/plc/FC_LEBT_collision_avoidance.plc"
dbLoadRecords "$(E3_CMD_TOP)/db/FC_CollAvoidStatus.db" "IOC=$(IOC):, P=$(LEBTFC):, SN1808=s001"

dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_HV.db" "IOC=$(IOC), HV=$(MEBTFC), SN3174=s013, SN4134=s014, SN2124=s016"
dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_MCU.db" "IOC=$(IOC), MCU=$(MEBTFC), SN1808=s010, SN2819=s011"
dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_MPSsignals.db" "IOC=$(IOC), Sys=$(MEBTFC), SN1808=s010, SN3174=s013"
dbLoadRecords "$(E3_CMD_TOP)/db/FC_Vac_Bias_ilock.db" "HV=$(MEBTFC), PORT=MC_CPU1, ASYN_NAME=plcs.plc1.static.MEBTreqbias"

$(SCRIPTEXEC) "$(ecmccfg_DIR)/loadPLCFile.cmd" "PLC_ID=1, SAMPLE_RATE_MS=10, FILE=$(E3_CMD_TOP)/plc/FC_MEBT_vacuum-HV_ilock.plc"
dbLoadRecords "$(E3_CMD_TOP)/db/limitswitchalarm.db" "P=$(MEBTFC)"

dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_HV.db" "IOC=$(IOC), HV=$(DTL2FC), SN3174=s022, SN4134=s023, SN2124=s025"
dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_MCU.db" "IOC=$(IOC), MCU=$(DTL2FC), SN1808=s019, SN2819=s020"
dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_MPSsignals.db" "IOC=$(IOC), Sys=$(DTL2FC), SN1808=s019, SN3174=s022"
dbLoadRecords "$(E3_CMD_TOP)/db/FC_Vac_Bias_ilock.db" "HV=$(DTL2FC), PORT=MC_CPU1, ASYN_NAME=plcs.plc2.static.DTL2reqbias"

$(SCRIPTEXEC) "$(ecmccfg_DIR)/loadPLCFile.cmd" "PLC_ID=2, SAMPLE_RATE_MS=10, FILE=$(E3_CMD_TOP)/plc/FC_DTL2_vacuum-HV_ilock.plc"
dbLoadRecords "$(E3_CMD_TOP)/db/limitswitchalarm.db" "P=$(DTL2FC)"

dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_HV.db" "IOC=$(IOC), HV=$(DTL4FC), SN3174=s031, SN4134=s032, SN2124=s034"
dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_MCU.db" "IOC=$(IOC), MCU=$(DTL4FC), SN1808=s028, SN2819=s029"
dbLoadRecords "$(E3_CMD_TOP)/db/Generic_FC_MPSsignals.db" "IOC=$(IOC), Sys=$(DTL4FC), SN1808=s028, SN3174=s031"
dbLoadRecords "$(E3_CMD_TOP)/db/FC_Vac_Bias_ilock.db" "HV=$(DTL4FC), PORT=MC_CPU1, ASYN_NAME=plcs.plc3.static.DTL4reqbias"

$(SCRIPTEXEC) "$(ecmccfg_DIR)/loadPLCFile.cmd" "PLC_ID=3, SAMPLE_RATE_MS=10, FILE=$(E3_CMD_TOP)/plc/FC_DTL4_vacuum-HV_ilock.plc"
dbLoadRecords "$(E3_CMD_TOP)/db/limitswitchalarm.db" "P=$(DTL4FC)"

dbLoadRecords "$(E3_CMD_TOP)/db/FC_HealthStatus.db" "P=$(IOC):, IOC=$(IOC):"

# END of ADDITIONAL SETUP

# Go active
$(SCRIPTEXEC) "$(ecmccfg_DIR)/setAppMode.cmd"

# Apply Access Security Group values
# See https://gitlab.esss.lu.se/e3/wrappers/core/e3-auth/blob/master/template/fc_security.acf
set_pass0_restoreFile "$(E3_CMD_TOP)/autosave/access_security.sav"
epicsEnvSet ASG_FILENAME fc_security.acf

# Run essioc snippet
$(SCRIPTEXEC) "$(essioc_DIR)/common_config.iocsh"
